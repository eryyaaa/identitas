package com.emerio.rnd.identitas.identitas;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;

import com.emerio.rnd.identitas.identitas.entity.Identitas;
import com.emerio.rnd.identitas.identitas.repo.IdentitasRepo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class IdentitasApplicationTests {
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
	MediaType.APPLICATION_JSON.getSubtype(),
	Charset.forName("utf8"));

	private MockMvc mockMvc;

	private String URIidentitas = "/identitas";

	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private IdentitasRepo identitasRepo;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
			.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
			.findAny()
			.orElse(null);

		assertNotNull("the JSON message converter must not be null",
				this.mappingJackson2HttpMessageConverter);
	}

	
	private Identitas identitas1;
	private Identitas identitas2;
	private Identitas identitas3;
	private Identitas identitas4;
	private Identitas identitas5;
	private Identitas identitas6;
	private Identitas identitas7;
	private Identitas identitas8;

	@Before()
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();


		this.identitas1 = new Identitas();
		this.identitas1.setNama("koplak1");
		this.identitas1.setUserName("koplo1");
		this.identitas1.setEmail("koplak@koplo.com");
		this.identitas1.setAlamat("dirumah1");
		this.identitas1.setUmur(19);
		this.identitas1.setJenisKelamin("pria");
		// this.identitas1.setTanggal(new Date(System.currentTimeMillis()));

		this.identitas2 = new Identitas();
		this.identitas2.setNama("koplak1");

		this.identitas3 = new Identitas();
		this.identitas3.setNama("koplak3");
		this.identitas3.setUserName("koplo3");
		this.identitas3.setEmail("koplakkoplo.com");
		this.identitas3.setAlamat("dirumah3");
		this.identitas3.setUmur(19);
		this.identitas3.setJenisKelamin("pria");

		this.identitas4 = new Identitas();
		this.identitas4.setNama("koplak4");
		this.identitas4.setUserName("koplo4");
		this.identitas4.setEmail("koplak4@koplo.com");
		this.identitas4.setAlamat("dirumah4");
		this.identitas4.setUmur(17);
		this.identitas4.setJenisKelamin("pria");

		this.identitas5 = new Identitas();
		this.identitas5.setNama("koplak5");
		this.identitas5.setUserName("koplo5");
		this.identitas5.setEmail("koplak5@koplo.com");
		this.identitas5.setAlamat("dirumah5");
		this.identitas5.setUmur(19);
		this.identitas5.setJenisKelamin("cowo");

		this.identitas6 = new Identitas();
		this.identitas6.setNama("koplak1");
		this.identitas6.setUserName("koplo1");
		this.identitas6.setEmail("koplak@koplo.com");
		this.identitas6.setAlamat("dirumah1");
		this.identitas6.setUmur(19);
		this.identitas6.setJenisKelamin("pria");

		this.identitas7 = new Identitas();
		this.identitas7.setNama("koplak7");
		this.identitas7.setUserName("koplo7");
		this.identitas7.setEmail("koplak7@koplo.com");
		this.identitas7.setAlamat("dirumah7");
		this.identitas7.setUmur(19);
		this.identitas7.setJenisKelamin("pria");
		// this.identitas7.setTanggal(new Date(System.currentTimeMillis()));

		this.identitas8 = new Identitas();
		this.identitas8.setNama("koplak8");
		this.identitas8.setUserName("koplo8");
		this.identitas8.setEmail("koplak8@koplo.com");
		this.identitas8.setAlamat("dirumah8");
		this.identitas8.setUmur(19);
		this.identitas8.setJenisKelamin("pria");
		// this.identitas8.setTanggal(new Date(System.currentTimeMillis()));
		
		this.identitas1 = identitasRepo.save(identitas1);
		this.identitas7 = identitasRepo.save(identitas7);
	}

	//contextload test
	@Test
	public void contextLoads() throws Exception {
		IdentitasApplication.main(new String[] {});
	}

	//positive test create
	@Test
	public void CreateUser() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas8))
				.contentType(contentType))
				.andExpect(status().isCreated());
	}

	//negative test create
	//sebagian attribut
	@Test
	public void CreateUserAttribut() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas2))
				.contentType(contentType))
				.andExpect((status().isBadRequest()));
	}

	//tanpa @ pada email
	@Test
	public void CreateNoAt() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas3))
				.contentType(contentType))
				.andExpect((status().isBadRequest()));
	}

	//jenis kelamin selain pria atau wanita
	@Test
	public void CreateBedaKelamin() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas5))
				.contentType(contentType))
				.andExpect((status().isBadRequest()));
	}

	//umur dibawah 18 tahun
	@Test
	public void CreateLowAge() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas4))
				.contentType(contentType))
				.andExpect((status().isBadRequest()));
	}

	//identitas sudah terdaftar

	//positive test read
	//read all user
	@Test
	public void readAll() throws Exception{
		ResultActions ra = mockMvc.perform(get(this.URIidentitas));
		ra.andExpect(content().contentType(contentType));
		ra.andExpect(status().isOk());
	}

	//read salah satu user
	@Test
	public void readOne() throws Exception{
		ResultActions ra = mockMvc.perform(get(this.URIidentitas + "/" + identitas7.getId()));
		ra.andExpect(content().contentType(contentType));
		ra.andExpect(content().json(this.json(identitas7)));
		ra.andExpect(status().isOk());
	}

	//negative test read
	@Test
	public void readNotHere() throws Exception{
		ResultActions ra = mockMvc.perform(get(this.URIidentitas + "/0"));
		ra.andExpect(status().isNotFound());
	}

	//positive test update
	//update semua attribut
	@Test
	public void UpdateAll() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/" + identitas1.getId())
		.content(this.json(identitas6)).contentType(contentType))
		.andExpect(status().isCreated());
	}

	//update sebagian attribut
	@Test
	public void UpdateSebagian() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/" + identitas1.getId())
		.content(this.json(identitas2)).contentType(contentType))
		.andExpect(status().isCreated());
	}

	//negative test update
	//update email tanpa @
	@Test
	public void UpdateNoAt() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/" + identitas1.getId())
		.content(this.json(identitas3)).contentType(contentType))
		.andExpect(status().isBadRequest());
	}

	//update umur dibawah 18th
	@Test
	public void UpdateLowAge() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/" + identitas1.getId())
		.content(this.json(identitas4)).contentType(contentType))
		.andExpect(status().isBadRequest());
	}

	//update yang tidak terdaftar
	@Test
	public void UpdateNoReg() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/100")
		.content(this.json(identitas5)).contentType(contentType))
		.andExpect(status().isNotFound());
	}

	//positive test delete
	@Test
	public void DeleteUser() throws Exception{
		mockMvc.perform(delete(this.URIidentitas + "/" + identitas7.getId().toString())
		.contentType(contentType)).andExpect(status().isOk());
	}

	//negative test delete
	@Test
	public void DeleteNotFound() throws Exception{
		mockMvc.perform(delete(this.URIidentitas + "/100").contentType(contentType))
		.andExpect(status().isNotFound());
	}

	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
