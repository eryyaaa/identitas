package com.emerio.rnd.identitas.identitas.repo;

import java.util.Optional;
import com.emerio.rnd.identitas.identitas.entity.Identitas;
import org.springframework.data.repository.CrudRepository;

public interface IdentitasRepo extends CrudRepository<Identitas, Long>{
    Optional<Identitas> findByNama(String nama);
}