package com.emerio.rnd.identitas.identitas.controller;

import java.util.Optional;

import com.emerio.rnd.identitas.identitas.entity.Identitas;
import com.emerio.rnd.identitas.identitas.repo.IdentitasRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;



@RestController
public class IdentitasController{

    private IdentitasRepo identitasRepo;
    @Autowired
    IdentitasController (IdentitasRepo identitasRepo){
        this.identitasRepo = identitasRepo;
    }

    @RequestMapping(method = RequestMethod.GET, value="/identitas", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Iterable<Identitas>> getAll ()
    {
        try
        {
            return new ResponseEntity<Iterable<Identitas>>(this.identitasRepo.findAll(), HttpStatus.OK);
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value="/identitas/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Identitas> getOne (@PathVariable Long id)
    {
        try
        {
            return new ResponseEntity<>(this.identitasRepo.findById(id).get() , HttpStatus.OK);
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value="/identitas", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Identitas> createOne(@RequestBody Identitas iden)
    {
        try
        {
            Optional<Identitas> identitas = this.identitasRepo.findByNama(iden.getNama());
            if (identitas.isPresent())
            {
                return new ResponseEntity<>(new Identitas(), HttpStatus.BAD_REQUEST);
            } else 
            {
                Identitas newIden =  this.identitasRepo.save(iden);
                return new ResponseEntity<>(newIden, HttpStatus.CREATED);
            }
        } catch (Exception e )
        {
            
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value="/identitas/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Identitas> updateOne (@PathVariable Long id, @RequestBody Identitas iden)
    {
        try 
        {
            Optional<Identitas> identitas = this.identitasRepo.findById(id);
            if (identitas.isPresent())
            {
                Identitas idenUpdate = identitas.get();
                if (iden.getNama() != null)
                {
                    idenUpdate.setNama(iden.getNama());
                }
                if (iden.getAlamat() != null)
                {
                    idenUpdate.setAlamat(iden.getAlamat());
                }
                if (iden.getEmail() != null)
                {
                    idenUpdate.setEmail(iden.getEmail());
                }
                if (iden.getUserName() != null)
                {
                    idenUpdate.setUserName(iden.getUserName());
                }
                if (iden.getUmur() != null)
                {
                    idenUpdate.setUmur(iden.getUmur());
                }
                if (iden.getJenisKelamin() != null)
                {
                    idenUpdate.setJenisKelamin(iden.getJenisKelamin());
                }
                idenUpdate = this.identitasRepo.save(idenUpdate);
                return new ResponseEntity<>(idenUpdate, HttpStatus.CREATED);
            } else 
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/identitas/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Identitas> deleteOne (@PathVariable Long id)
    {
        try
        {
            Optional<Identitas> idenDelete = this.identitasRepo.findById(id);
            if (idenDelete.isPresent())
            {
                this.identitasRepo.delete(idenDelete.get());
                return new ResponseEntity<>(idenDelete.get(), HttpStatus.OK);
            } else
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e )
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
