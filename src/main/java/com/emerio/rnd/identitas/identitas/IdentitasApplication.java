package com.emerio.rnd.identitas.identitas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdentitasApplication {

	public static void main(String[] args) {
		SpringApplication.run(IdentitasApplication.class, args);
	}
}
