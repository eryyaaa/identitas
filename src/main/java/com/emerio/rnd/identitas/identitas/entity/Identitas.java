package com.emerio.rnd.identitas.identitas.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Identitas{
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String nama;

    @NotEmpty
    private String username;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Pattern(regexp="pria|wanita")
    private String jeniskelamin;

    @NotNull
    @Min(18)
    private Integer umur;

    @NotEmpty
    private String alamat;

    // @NotNull
    // @DateTimeFormat(pattern="dd/MM/YY")
    // private Date tanggal;

    //getter and setter

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getUserName(){
        return this.username;
    }

    public void setUserName(String userName){
        this.username = userName;
    }

    public String getEmail(){
        return this.email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public Integer getUmur(){
        return this.umur;
    }

    public void setUmur(Integer umur){
        this.umur = umur;
    }

    public String getJenisKelamin(){
        return this.jeniskelamin;
    }

    public void setJenisKelamin(String jenisKelamin){
        this.jeniskelamin = jenisKelamin;
    }

    public String getAlamat(){
        return this.alamat;
    }

    public void setAlamat(String alamat){
        this.alamat = alamat;
    }

    // public Date getTanggal(){
    //     return this.tanggal;
    // }

    // public void setTanggal(Date tanggal){
    //     this.tanggal = tanggal;
    // }

}