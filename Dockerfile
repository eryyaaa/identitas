FROM openjdk:8-jdk-alpine
ADD ./target/*.jar identity-tdd.jar
ADD ./identity-tdd.sh identity-tdd.sh
RUN ["chmod", "+x", "/identity-tdd.sh"]
ENTRYPOINT ["/identity-tdd.sh"]
