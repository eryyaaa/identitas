## TDD (Test Driven Development)
merupakan metode pemrograman dimana program diarahkan oleh test, maksudnya adalah pembuatan working code didalam program akan dibuat setelah test dilakukan. implementasi dari tdd adalah
1. membuat test apa saja yang akan dijalankan. pastikan beberapa kemungkinan yang dapat ditemukan, kemungkinan yang didapat bisa berupa positive test atau negative test.
2. jalankan testnya, tetapi pastikan test yang dilakukan failure karena belum ada working code.
3. buat working code seminimal mungkin sesuai dengan case yang dibuat.
4. pastikan test yang dilakukan pass hingga build success.
5. ulangi langkah diatas hingga selesai.

## Dependencies
### WEB

WEB digunakan sebagai agar code yang digunakan dapat terhubung ke web.

### H2

H2 ada sebagai sistem management dari database yang dijalankan pada java.

### JPA

JPA atau java presistence API menggunakan ORM dimana menjadi standart bagi ORM dalam java. Digunakan untuk memanipulasi data yang digunakan tanpa menggunakan query.

***
## TDD Menerapkan

### ORM
ORM atau Object relational mapping merupakan metode yang digunakan untuk mengatur data pada database melalui java tanpa menyentuh tabel database secara langsung.

### CRUD Repository
maksud dari CRUD disini merupakan *Create, Read, Update,* dan *Delete*. Repository yang akan dibuat akan meng-implementasi CRUD. CRUD Repository sendiri adalah sebuah *interface* yang menyediakan method yang digunakan untuk menjalankan CRUD pada suatu entity.

## Implementasi TDD
1. Overview Project

Project kali ini akan membuat project yang meng-implementasikan CRUD dengan menggunakan spring-boot. *Entity* yang akan dibuat adalah identitas dimana terdapat attribut nama, username, email, umur, jenis kelamin, dan alamat. 

2. Skema Test

Berdasarkan isi *entity* yang dibuat, maka dapat dibuat juga skema test yang akan dilakukan pada test kali ini.

|Fungsi|Positive|Negative|
|:---:|:---:|:---:|
|Create|Membuat sebuah identitas dengan seluruh attribut|Membuat sebuah identitas dengan sebagian attribut|
| | |Membuat sebuah identitas tanpa "@" pada email|
| | |Membuat sebuah identitas dengan umur dibawah 18 tahun|
| | |Membuat sebuah identitas dengan mengisi jenis kelamin selain pria dan wanita|
| | |Membuat sebuah identitas dengan nama atau email yang sudah ada|
|Read|Menampilkan seluruh identitas yang tersimpan|Menampilkan identitas yang tidak terdaftar|
| |Menampilkan salah satu identitas yang tersimpan| |
|Update|Mengupdate semua attribut identitas|Update sebuah identitas dengan nama dan email yang sudah ada|
| |Mengupdate salah satu attribut identitas|Update sebuah identitas tanpa "@" pada email|
| | |Update sebuah identitas dengan umur dibawah 18 tahun|
| | |Update sebuah identitas yang tidak terdaftar|
|Delete|Menghapus identitas yang tersedia|Menghapus identitas yang tidak tersedia|

## Generate Project

Untuk membuat project baru yang akan diimplementasikan TDD didalamnya, kita akan generate project dari start.spring.io . Setelah itu pilih maven project dengan java dan Spring boot 2. Lalu masukan Dependencies Web, H2, dan JPA. Sesudah menginput dependencies, masukkan nama group dan artifact yang akan digunakan. Lalu klik generate project, kita akan diberikan file berupa zip yang akan digunakan sebagai project. Extract dan masuk ke dalam direktori folder tersebut.

## Membuat class entity

Dikarenakan kita akan menjalankan sebuah test, maka kita memerlukan sebuah inputan berupa sebuah data identitas yang akan digunakan untuk test nanti. Class Entity menerapkan ORM (*Object Relational Mapping*) seperti yang sudah dijelaskan sebelumnya.
```
|-- .mvn 
|-- .settings
|-- src
	|-- main 
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- identitas
							|-- identitas
								|-- entity 
									|-- Identitas.java	
								|-- IdentitasApplication.java
			|-- resources
				|-- static
				|-- templates
				|-- application.properties
	|-- test
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- identitas
							|-- identitas
								|-- IdentitasApplicationTest.java
|-- target
|-- .classpath
|-- .gitignore
|-- .project
|-- mvnw
|-- mvnw.cmd
|-- pom.xml

```

## Class Entity

```java
package com.emerio.rnd.identitas.identitas.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class Identitas{
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String nama;

    @NotEmpty
    private String userName;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Pattern(regexp="pria|wanita")
    private String jenisKelamin;

    @NotNull
    @Min(18)
    private Integer umur;

    @NotEmpty
    private String alamat;

    //getter and setter

    public Long getId(){
        return this.id;
    }

    public void setId(Long id){
        this.id = id;
    }

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama){
        this.nama = nama;
    }

    public String getUserName(){
        return this.userName;
    }

    public void setUserName(String userName){
        this.userName = userName;
    }

    public String getEmail(){
        return this.email;
    }

    public void setEmail(String email){
        this.email = email;
    }

    public Integer getUmur(){
        return this.umur;
    }

    public void setUmur(Integer umur){
        this.umur = umur;
    }

    public String getJenisKelamin(){
        return this.jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin){
        this.jenisKelamin = jenisKelamin;
    }

    public String getAlamat(){
        return this.alamat;
    }

    public void setAlamat(String alamat){
        this.alamat = alamat;
    }

}
```

### Annotasi yang digunakan

Di dalam class Entity digunakan beberapa annotasi yang diperlukan untuk terhubung dengan spring boot, annotasi yang digunakan adalah sebagai berikut:

- `@Entity` adalah annotasi yang digunakan untuk menandakan bahwa class tersebut adalah *entity class*
- `@Id` adalah annotasi yang digunakan sebagai tanda bahwa entitas tersebut merupakan *primary key* dari semua entitas yang ada
- `@GeneratedValue` adalah annotasi yang digunakan pada primary key, agar primary key dapat auto generated setiap ada sebuah data baru yang dibuat
- `@NotEmpty` adalah annotasi yang digunakan sebagai validasi data input agar tidak null, size dari input harus lebih dari 0
- `@NotNull` adalah annotasi yang digunakan sebagai validasi data input agar tidak null, tetapi boleh kosong
- `@Email` adalah annotasi yang digunakan sebagai validasi data input yang berupa email, input email yang diberikan tidak boleh tanpa @ pada email
- `@Pattern` adalah annotasi agar isi dari input dapat sama dengan *regular expression* yang ditetapkan
- `@Min` adalah annotasi untuk memberikan inputan berupa `BigDecimal`,`BigInteger`,`byte`,`short`,`long`,`int` dan jumlah inputan harus lebih besar dari angka minimal yang ditetapkan

## Membuat CRUD Repository

CRUD Repository merupakan class interface dan extends dari Spring repository interface. Interface ini berguna untuk menyediakan method yang akan digunakan untuk operasi standard CRUD pada suatu entity. CRUD sendiri memiliki arti *Create*, *Read*, *Update*, dan *Delete*.

```
|-- .mvn 
|-- .settings
|-- src
	|-- main 
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- identitas
							|-- identitas
								|-- entity 
									|-- Identitas.java
								|-- repo
									|-- IdentitasRepo.java	
								|-- IdentitasApplication.java
			|-- resources
				|-- static
				|-- templates
				|-- application.properties
	|-- test
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- identitas
							|-- identitas
								|-- IdentitasApplicationTest.java
|-- target
|-- .classpath
|-- .gitignore
|-- .project
|-- mvnw
|-- mvnw.cmd
|-- pom.xml

```

## CRUD Repository

```java
package com.emerio.rnd.identitas.identitas.repo;

import java.util.Optional;
import com.emerio.rnd.identitas.identitas.entity.Identitas;
import org.springframework.data.repository.CrudRepository;

public interface IdentitasRepo extends CrudRepository<Identitas, Long>{
    Optional<Identitas> findByNama(String nama);
}
```
## Membuat Class Test

Class ini dibuat untuk menjalankan test pada TDD. Isi dari class ini adalah beberapa unit test yang akan digunakan untuk menjalankan test. Jumlah test yang digunakan berdasarkan skenario yang sudah dibuat sebelumnya.

```
|-- .mvn 
|-- .settings
|-- src
	|-- main 
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- identitas
							|-- identitas
								|-- entity 
									|-- Identitas.java
								|-- repo
									|-- IdentitasRepo.java	
								|-- IdentitasApplication.java
			|-- resources
				|-- static
				|-- templates
				|-- application.properties
	|-- test
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- identitas
							|-- identitas
								|-- IdentitasApplicationTest.java
|-- target
|-- .classpath
|-- .gitignore
|-- .project
|-- mvnw
|-- mvnw.cmd
|-- pom.xml

```


## Class Application test

```java
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
    public class IdentitasApplicationTests {
	    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
	    MediaType.APPLICATION_JSON.getSubtype(),
	    Charset.forName("utf8"));

	    private MockMvc mockMvc;

	    private String URIidentitas = "/identitas";

	    private HttpMessageConverter mappingJackson2HttpMessageConverter;

	    @Autowired
	    private IdentitasRepo identitasRepo;

	    @Autowired
	    private WebApplicationContext webApplicationContext;

	    @Autowired
	    void setConverters(HttpMessageConverter<?>[] converters) {
            this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
			.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
			.findAny()
			.orElse(null);

		assertNotNull("the JSON message converter must not be null",
				this.mappingJackson2HttpMessageConverter);
	}

	
	private Identitas identitas1;
	private Identitas identitas2;
	private Identitas identitas3;
	private Identitas identitas4;
	private Identitas identitas5;
	private Identitas identitas6;
	private Identitas identitas7;
	private Identitas identitas8;

	@Before()
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();


		this.identitas1 = new Identitas();
		this.identitas1.setNama("koplak1");
		this.identitas1.setUserName("koplo1");
		this.identitas1.setEmail("koplak@koplo.com");
		this.identitas1.setAlamat("dirumah1");
		this.identitas1.setUmur(19);
		this.identitas1.setJenisKelamin("pria");

		this.identitas2 = new Identitas();
		this.identitas2.setNama("koplak1");

		this.identitas3 = new Identitas();
		this.identitas3.setNama("koplak3");
		this.identitas3.setUserName("koplo3");
		this.identitas3.setEmail("koplakkoplo.com");
		this.identitas3.setAlamat("dirumah3");
		this.identitas3.setUmur(19);
		this.identitas3.setJenisKelamin("pria");

		this.identitas4 = new Identitas();
		this.identitas4.setNama("koplak4");
		this.identitas4.setUserName("koplo4");
		this.identitas4.setEmail("koplak4@koplo.com");
		this.identitas4.setAlamat("dirumah4");
		this.identitas4.setUmur(17);
		this.identitas4.setJenisKelamin("pria");

		this.identitas5 = new Identitas();
		this.identitas5.setNama("koplak5");
		this.identitas5.setUserName("koplo5");
		this.identitas5.setEmail("koplak5@koplo.com");
		this.identitas5.setAlamat("dirumah5");
		this.identitas5.setUmur(19);
		this.identitas5.setJenisKelamin("cowo");

		this.identitas6 = new Identitas();
		this.identitas6.setNama("koplak1");
		this.identitas6.setUserName("koplo1");
		this.identitas6.setEmail("koplak@koplo.com");
		this.identitas6.setAlamat("dirumah1");
		this.identitas6.setUmur(19);
		this.identitas6.setJenisKelamin("pria");

		this.identitas7 = new Identitas();
		this.identitas7.setNama("koplak7");
		this.identitas7.setUserName("koplo7");
		this.identitas7.setEmail("koplak7@koplo.com");
		this.identitas7.setAlamat("dirumah7");
		this.identitas7.setUmur(19);
		this.identitas7.setJenisKelamin("pria");

		this.identitas8 = new Identitas();
		this.identitas8.setNama("koplak8");
		this.identitas8.setUserName("koplo8");
		this.identitas8.setEmail("koplak8@koplo.com");
		this.identitas8.setAlamat("dirumah8");
		this.identitas8.setUmur(19);
		this.identitas8.setJenisKelamin("pria");
		
		this.identitas1 = identitasRepo.save(identitas1);
		this.identitas7 = identitasRepo.save(identitas7);
	}

	//contextload test
	@Test
	public void contextLoads() throws Exception {
		IdentitasApplication.main(new String[] {});
	}

	//positive test create
	@Test
	public void CreateUser() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas8))
				.contentType(contentType))
				.andExpect(status().isCreated());
	}

	//negative test create
	//sebagian attribut
	@Test
	public void CreateUserAttribut() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas2))
				.contentType(contentType))
				.andExpect((status().isBadRequest()));
	}

	//tanpa @ pada email
	@Test
	public void CreateNoAt() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas3))
				.contentType(contentType))
				.andExpect((status().isBadRequest()));
	}

	//jenis kelamin selain pria atau wanita
	@Test
	public void CreateBedaKelamin() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas5))
				.contentType(contentType))
				.andExpect((status().isBadRequest()));
	}

	//umur dibawah 18 tahun
	@Test
	public void CreateLowAge() throws Exception{
		mockMvc.perform(post(this.URIidentitas)
				.content(this.json(identitas4))
				.contentType(contentType))
				.andExpect((status().isBadRequest()));
	}

	//identitas sudah terdaftar

	//positive test read
	//read all user
	@Test
	public void readAll() throws Exception{
		ResultActions ra = mockMvc.perform(get(this.URIidentitas));
		ra.andExpect(content().contentType(contentType));
		ra.andExpect(status().isOk());
	}

	//read salah satu user
	@Test
	public void readOne() throws Exception{
		ResultActions ra = mockMvc.perform(get(this.URIidentitas + "/" + identitas7.getId()));
		ra.andExpect(content().contentType(contentType));
		ra.andExpect(content().json(this.json(identitas7)));
		ra.andExpect(status().isOk());
	}

	//negative test read
	@Test
	public void readNotHere() throws Exception{
		ResultActions ra = mockMvc.perform(get(this.URIidentitas + "/0"));
		ra.andExpect(status().isNotFound());
	}

	//positive test update
	//update semua attribut
	@Test
	public void UpdateAll() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/" + identitas1.getId())
		.content(this.json(identitas6)).contentType(contentType))
		.andExpect(status().isCreated());
	}

	//update sebagian attribut
	@Test
	public void UpdateSebagian() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/" + identitas1.getId())
		.content(this.json(identitas2)).contentType(contentType))
		.andExpect(status().isCreated());
	}

	//negative test update
	//update email tanpa @
	@Test
	public void UpdateNoAt() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/" + identitas1.getId())
		.content(this.json(identitas3)).contentType(contentType))
		.andExpect(status().isBadRequest());
	}

	//update umur dibawah 18th
	@Test
	public void UpdateLowAge() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/" + identitas1.getId())
		.content(this.json(identitas4)).contentType(contentType))
		.andExpect(status().isBadRequest());
	}

	//update yang tidak terdaftar
	@Test
	public void UpdateNoReg() throws Exception{
		mockMvc.perform(put(this.URIidentitas + "/100")
		.content(this.json(identitas5)).contentType(contentType))
		.andExpect(status().isNotFound());
	}

	//positive test delete
	@Test
	public void DeleteUser() throws Exception{
		mockMvc.perform(delete(this.URIidentitas + "/" + identitas7.getId().toString())
		.contentType(contentType)).andExpect(status().isOk());
	}

	//negative test delete
	@Test
	public void DeleteNotFound() throws Exception{
		mockMvc.perform(delete(this.URIidentitas + "/100").contentType(contentType))
		.andExpect(status().isNotFound());
	}

	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
```

### Annotasi yang digunakan

- `@RunWith` adalah annotasi yang digunakan untuk menandakan bahwa test akan dilakukan pada *class* tersebut
- `@SpringBootTest` adalah annotasi yang digunakan untuk menjelaskan bahwa *class test* yang digunakan adalah *Spring boot based test* itu berarti test yang dilakukan menggunakan spring boot
- `@WebAppConfiguration` adalah annotasi yang digunakan untuk mendeklarasi ApplicationContext(interface untuk configurasi web application) yang akan terintegrasi dengan test 
- `@Autowired` adalah annotasi yang digunakan sebagai penanda constructor, field, atau configurasi method agar dapat melakukan dependencies injection dari class lain
- `@Before` adalah annotasi yang digunakan saat akan membuat sebuah test. Method yang diberikan annotasi `@Before` akan dijalankan sebelum method yang diberikan annotasi `@Test`
- `@Test` adalah annotasi yang digunakan untuk menandakan bahwa method tersebut merupakan method yang dijalankan untuk melakukan test

## Membuat Controller

Bagian terakhir pada pembuatan program ini adalah pembuatan class Controller. Class ini berguna sebagai *Working Code* pada program dimana class ini berisi 4 buah fungsi sesuai dengan project yang dikerjakan yaitu *create*, *read*, *update*, dan *delete*.


```
|-- .mvn 
|-- .settings
|-- src
	|-- main 
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- identitas
							|-- identitas
								|-- controller
									|-- IdentitasController.java
								|-- entity 
									|-- Identitas.java
								|-- repo
									|-- IdentitasRepo.java	
								|-- IdentitasApplication.java
			|-- resources
				|-- static
				|-- templates
				|-- application.properties
	|-- test
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- identitas
							|-- identitas
								|-- IdentitasApplicationTest.java
|-- target
|-- .classpath
|-- .gitignore
|-- .project
|-- mvnw
|-- mvnw.cmd
|-- pom.xml

```

## Class Controller

```java
package com.emerio.rnd.identitas.identitas.controller;

import java.util.Optional;

import com.emerio.rnd.identitas.identitas.entity.Identitas;
import com.emerio.rnd.identitas.identitas.repo.IdentitasRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;



@RestController
public class IdentitasController{

    private IdentitasRepo identitasRepo;
    @Autowired
    IdentitasController (IdentitasRepo identitasRepo){
        this.identitasRepo = identitasRepo;
    }

    @RequestMapping(method = RequestMethod.GET, value="/identitas", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Iterable<Identitas>> getAll ()
    {
        try
        {
            return new ResponseEntity<Iterable<Identitas>>(this.identitasRepo.findAll(), HttpStatus.OK);
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value="/identitas/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Identitas> getOne (@PathVariable Long id)
    {
        try
        {
            return new ResponseEntity<Identitas>(this.identitasRepo.findById(id).get() , HttpStatus.OK);
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value="/identitas", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Identitas> createOne(@RequestBody Identitas iden)
    {
        try
        {
            Optional<Identitas> identitas = this.identitasRepo.findByNama(iden.getNama());
            if (identitas.isPresent())
            {
                return new ResponseEntity<Identitas>(new Identitas(), HttpStatus.BAD_REQUEST);
            } else 
            {
                Identitas newIden =  this.identitasRepo.save(iden);
                return new ResponseEntity<Identitas>(newIden, HttpStatus.CREATED);
            }
        } catch (Exception e )
        {
            
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value="/identitas/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Identitas> updateOne (@PathVariable Long id, @RequestBody Identitas iden)
    {
        try 
        {
            Optional<Identitas> identitas = this.identitasRepo.findById(id);
            if (identitas.isPresent())
            {
                Identitas idenUpdate = identitas.get();
                if (iden.getNama() != null)
                {
                    idenUpdate.setNama(iden.getNama());
                }
                if (iden.getAlamat() != null)
                {
                    idenUpdate.setAlamat(iden.getAlamat());
                }
                if (iden.getEmail() != null)
                {
                    idenUpdate.setEmail(iden.getEmail());
                }
                if (iden.getUserName() != null)
                {
                    idenUpdate.setUserName(iden.getUserName());
                }
                if (iden.getUmur() != null)
                {
                    idenUpdate.setUmur(iden.getUmur());
                }
                if (iden.getJenisKelamin() != null)
                {
                    idenUpdate.setJenisKelamin(iden.getJenisKelamin());
                }
                idenUpdate = this.identitasRepo.save(idenUpdate);
                return new ResponseEntity<Identitas>(idenUpdate, HttpStatus.CREATED);
            } else 
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/identitas/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Identitas> deleteOne (@PathVariable Long id)
    {
        try
        {
            Optional<Identitas> idenDelete = this.identitasRepo.findById(id);
            if (idenDelete.isPresent())
            {
                this.identitasRepo.delete(idenDelete.get());
                return new ResponseEntity<Identitas>(idenDelete.get(), HttpStatus.OK);
            } else
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e )
        {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
```

### Annotasi yang digunakan

- `@RestController` adalah annotasi yang digunakan sebagai penanda bahwa class tersebut merupakan class controller
- `@RequestMapping` adalah annotasi yang digunakan agar dapat mengatur mapping request web ke dalam method
- `@PathVariable` adalah annotasi yang digunakan sebagai penanda variable yang akan di panggil dari URI oleh controller
- `@RequestBody` adalah annotasi yang digunakan menentukan bentuk body dari sebuah program pada saat akan dijalankan nanti